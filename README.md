**Poker Hand Comparison**

This Project follows the instructions from [Poker Hand Comparison](https://bitbucket.org/germaingrabyo/js-kata/src/master/) and accord to the rules of [Texas Hold'em Rules](https://en.wikipedia.org/wiki/Texas_hold_%27em#Hand_values).

---

## Program

To execute this program, just write *java -jar PokerHandComparison.jar* in a command line interface.
Then follow the instructions :

1. With **manual**, you have to enter your own poker hands. 
2. With **automatic**, the program will ran tests already write.

*If you want to modify the tests, open PokerHandTest.java and create a new .jar.*