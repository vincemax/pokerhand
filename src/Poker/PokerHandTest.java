package Poker;

import java.util.Scanner;

public class PokerHandTest {

	public static void main(String args[]) {

		String again = "y";

		while (again.equals("y")) {

			@SuppressWarnings("resource")
			Scanner s = new Scanner(System.in);
			System.out.println("Test : Manual or Automatic ? (1/2)");
			String test = s.nextLine();

			if (test.equals("1")) {
				// user choose hands
				System.out.println(
						"\nRequirements :\n- A space is used as card separator.\n- First caracter is the value of the card (2,3,4,5,6,7,8,9,T,J,Q,K,A).\n- Second character is the suit (S,H,D,C).\nExemple : 5H 6D KC 5S QS.\n");
				System.out.print("Hand 1 : ");
				PokerHand Hand1 = new PokerHand(s.nextLine());
				System.out.print("Hand 2 : ");
				PokerHand Hand2 = new PokerHand(s.nextLine());
				@SuppressWarnings("unused")
				Result H1vsH2 = Hand1.compareWith(Hand2);
			} else if (test.equals("2")) {
				// run automatic tests
				PokerHand RoyalFlush = new PokerHand("TH AH JH KH QH");
				PokerHand StraightFlush = new PokerHand("8C TC 9C JC 7C");
				PokerHand FourOfAKind1 = new PokerHand("KC KH KS 7S KD");
				PokerHand FourOfAKind2 = new PokerHand("KC KH KS 5S KD");
				PokerHand FullHouse = new PokerHand("KC 7C KH KD 7S");
				PokerHand Flush = new PokerHand("KH 3H 2H 4H TH");
				PokerHand Straight = new PokerHand("2S 3C 5H AD 4S");
				PokerHand ThreeOfAKind = new PokerHand("KC KH 7C 5S KD");
				PokerHand TwoPairs = new PokerHand("KH 7D 7C KH 5S");
				PokerHand Pair = new PokerHand("5H 6H KC 5S QS");
				PokerHand HighCard = new PokerHand("AC 4H 7D KC 2C");

				// check if results are right
				Result RFvsTP = RoyalFlush.compareWith(TwoPairs);
				assert (RFvsTP.getResult() == 1);
				Result HvsFH = HighCard.compareWith(FullHouse);
				assert (HvsFH.getResult() == 2);
				Result FOKvsFOK = FourOfAKind1.compareWith(FourOfAKind2);
				assert (FOKvsFOK.getResult() == 1);
				Result SFvsSF = StraightFlush.compareWith(StraightFlush);
				assert (SFvsSF.getResult() == 3);
				Result SvsF = Straight.compareWith(Flush);
				assert (SvsF.getResult() == 2);
				Result TOKvsP = ThreeOfAKind.compareWith(Pair);
				assert (TOKvsP.getResult() == 1);

			} else {
				System.out.println("Wrong answer.");
			}
			System.out.println("Continue ? (y/n)");
			again = s.nextLine();
		}
		System.out.println("Quit program.");
	}
}
