package Poker;

public enum Result {
	WIN(1), LOSS(2), TIE(3);

	int id;

	Result(int i) {
		this.id = i;
	}

	public int getResult() {
		return this.id;
	}
}
