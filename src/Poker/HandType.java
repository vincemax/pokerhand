package Poker;

public enum HandType {
	RoyalFlush(9), StraightFlush(8), FourOfAKind(7), FullHouse(6), Flush(5), Straight(4), ThreeOfAKind(3), TwoPairs(
			2), Pair(1), Highcard(0);
	int id;

	HandType(int i) {
		this.id = i;
	}

	public int getHandType() {
		return this.id;
	}
}
