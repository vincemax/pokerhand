package Poker;

public class PokerHand {

	String[] cards; // array with all cards
	int[] occurrences = new int[13]; // array size number of different cards
										// (occ[i] = number of card with value i-2)
	HandType HT = null;

	public PokerHand(String s) {
		int k = 0;
		cards = s.split("\\s");
		if (cards.length != 5) { // check if hand has the right number of cards
			System.out.println("Wrong number of card in a Hand.");
			System.exit(-1);
		} else {
			// Initialization
			char[] cardsnumbers = new char[5]; // array with numbers from cards of hand
			for (int i = 0; i < 5; i++) { // complete the array with values
				cardsnumbers[i] = this.cards[i].charAt(0);
				// check if suits are ok
				if ((this.cards[i].charAt(1) != 'H') && (this.cards[i].charAt(1) != 'S')
						&& (this.cards[i].charAt(1) != 'D') && (this.cards[i].charAt(1) != 'C')) {
					k++;
				}
			}

			if (k != 0) {
				// suits not ok
				System.out.println("Problem with suits in a Hand.");
				System.exit(-1);
			}

			// replace (T J Q K A) by (: ; < = >) to match with Decimal ASCII table
			for (int i = 0; i < 5; i++) {
				switch (cardsnumbers[i]) {
				case 'T':
					this.occurrences[8]++; // increment number of card value 10
					break;
				case 'J':
					this.occurrences[9]++; // increment number of card Jack
					break;
				case 'Q':
					this.occurrences[10]++; // increment number of card Queen
					break;
				case 'K':
					this.occurrences[11]++; // increment number of card King
					break;
				case 'A':
					this.occurrences[12]++; // increment number of card Ace
					break;
				default:
					// check if cards have right values (between 2 and 9)
					if ((cardsnumbers[i] - '0' >= 2) && (cardsnumbers[i] - '0' <= 9)) {
						this.occurrences[cardsnumbers[i] - '0' - 2]++; // increment number of card value i
					} else {
						System.out.println("Problem with values in a Hand.");
						System.exit(-1);
					}
					break;
				}
			}
		}
	}

	public void displayHand() { // display hand on terminal
		for (int i = 0; i < 5; i++) {
			System.out.print(this.cards[i]);
			System.out.print(" ");
		}
	}

	public int[] extractCardsnumbers() { // out : number from cards order by power (strongest last)
		int[] cardsnumbers = new int[5];
		int j = 0;
		for (int i = 0; i < 13; i++) {
			switch (this.occurrences[i]) {
			case 1:
				cardsnumbers[j] = i;
				j++;
				break;
			case 2:
				if ((cardsnumbers[4] != 0) && (cardsnumbers[3] != 0)) { // if already a pair or a three
					if (cardsnumbers[2] != 0) { // if already a three
						cardsnumbers[1] = i; // pair in first position
						cardsnumbers[0] = i;
					} else { // if already a pair
						cardsnumbers[2] = cardsnumbers[4]; // new pair stronger
						cardsnumbers[1] = cardsnumbers[3]; // so swip weak pair to the left
						cardsnumbers[4] = i; // new pair in strongest position
						cardsnumbers[3] = i;
					}
				} else { // if there is nothing stronger
					for (int k = 4; k > 2; k--) { // put pair in strongest position
						cardsnumbers[k] = i;
					}
				}
				break;
			case 3:
				if ((cardsnumbers[4] != 0) && (cardsnumbers[3] != 0)) { // if already a pair in, swip pair to the left
					cardsnumbers[1] = cardsnumbers[4];
					cardsnumbers[0] = cardsnumbers[3];
				}
				for (int k = 4; k > 1; k--) { // put the three in strongest position
					cardsnumbers[k] = i;
				}
				break;
			case 4:
				for (int k = 4; k > 0; k--) { // put the fourth in strongest position
					cardsnumbers[k] = i;
				}
				break;
			default:
				break;
			}
		}
		return cardsnumbers;
	}

	public void evaluateHand() { // set to the hand the type of hand

		int[] cardsnumbers = new int[5]; // array with values from cards of hand
		char[] cardssuits = new char[5]; // array with suits from cards of hand

		for (int i = 0; i < 5; i++) { // complete the array with suits
			cardssuits[i] = this.cards[i].charAt(1);
		}
		cardsnumbers = this.extractCardsnumbers(); // complete the array with values

		// 1st test : every card same suit
		int i = 0;
		while ((i < 4) && (cardssuits[i] == cardssuits[i + 1])) {
			i++;
		}

		// if all card same suit
		if (i == 4) {
			// 2nd test : straight
			i = 0;
			while ((i < 4) && (cardsnumbers[i] + 1 == cardsnumbers[i + 1])) {
				i++;
			}

			// if it's straight
			// if it's straight (check case if straight A 2 3 4 5)
			if ((i == 4) || ((i == 3) && (cardsnumbers[0] + 2 == 2) && (cardsnumbers[4] + 2 == 14))) {
				// 3rd test : royal or straight

				// if smallest card is ten
				if (cardsnumbers[0] + 2 == 10) {
					this.HT = HandType.RoyalFlush; // it's Royal Flush
				}

				// if smallest card isn't a ten
				else {
					this.HT = HandType.StraightFlush; // it's Straight Flush
				}
			}

			// if it's not straight
			else {
				this.HT = HandType.Flush; // it's Flush
			}
		}

		// if not all card same suit
		else {

			// 2nd test : occurrences
			// count the number of pairs, three and four
			int pair = 0;
			int three = 0;
			int four = 0;
			for (i = 0; i < 13; i++) {
				switch (this.occurrences[i]) {
				case 2:
					pair++;
					break;
				case 3:
					three++;
					break;
				case 4:
					four++;
					break;
				default:
					break;
				}
			}

			// if there is 4 same number
			if (four == 1) {
				this.HT = HandType.FourOfAKind; // it's Four of a kind
			}

			// if there isn't 4 same number
			else {

				// if there is 3 same number
				if (three == 1) {

					// if there is also a pair
					if (pair == 1) {
						this.HT = HandType.FullHouse; // it's a Full House
					}

					// if there is no pair
					else {
						this.HT = HandType.ThreeOfAKind; // it's Three of a kind
					}
				}

				// if there is no 3 same number
				else {

					// if there is two pairs
					if (pair == 2) {
						this.HT = HandType.TwoPairs; // it's Two pairs
					}

					// if there is one pair
					else if (pair == 1) {
						this.HT = HandType.Pair; // it'ss a pair
					}

					// if there is no pair
					else {
						// 3rd test : straight
						i = 0;
						while ((i < 4) && (cardsnumbers[i] + 1 == cardsnumbers[i + 1])) {
							i++;
						}

						// if it's straight (check case if straight A 2 3 4 5)
						if ((i == 4) || ((i == 3) && (cardsnumbers[0] + 2 == 2) && (cardsnumbers[4] + 2 == 14))) {
							this.HT = HandType.Straight; // it's Straight
						}

						// if it's not straight
						else {
							this.HT = HandType.Highcard; // it's Highcard
						}
					}
				}
			}
		}
	}

	public Result compareWith(PokerHand hand2) { // compare two hands hand return result from point of view of the first

		this.prettyPrint();
		System.out.println("VS");
		hand2.prettyPrint();

		if (this.HT.getHandType() > hand2.HT.getHandType()) { // hand 1 stronger than hand 2
			System.out.println("Hand 1 WIN.\n");
			return Result.WIN;
		} else if (this.HT.getHandType() < hand2.HT.getHandType()) { // hand 2 stronger than hand 1
			System.out.println("Hand 2 WIN.\n");
			return Result.LOSS;
		} else { // 2 hands same hand type

			int[] cardsnb1 = new int[5];
			int[] cardsnb2 = new int[5];
			cardsnb1 = this.extractCardsnumbers(); // values of hand 1
			cardsnb2 = hand2.extractCardsnumbers(); // values of hand 2

			int i = 4;
			while ((i > 0) && (cardsnb1[i] == cardsnb2[i])) { // compare all values of two hands and stop when different
				i--;
			}
			if (cardsnb1[i] > cardsnb2[i]) { // hand 1 has the highest cards
				System.out.println("Hand 1 WIN.\n");
				return Result.WIN;
			} else if (cardsnb1[i] < cardsnb2[i]) { // hand 2 has the highest cards
				System.out.println("Hand 2 WIN.\n");
				return Result.LOSS;
			} else { // two hands has the same cards so tie
				System.out.println("Draw match.\n");
				return Result.TIE;
			}
		}
	}

	public void prettyPrint() { // pretty print a hand on terminal

		if (this.HT == null) {
			this.evaluateHand();
		}
		int[] cardsnb = new int[5];
		char[] cards = new char[5];
		cardsnb = this.extractCardsnumbers();

		for (int i = 0; i < 5; i++) {
			switch (cardsnb[i]) {
			case 12:
				cards[i] = 'A';
				break;
			case 11:
				cards[i] = 'K';
				break;
			case 10:
				cards[i] = 'Q';
				break;
			case 9:
				cards[i] = 'J';
				break;
			case 8:
				cards[i] = 'T';
				break;
			default:
				cards[i] = (char) (cardsnb[i] + 2 + '0');
			}
		}

		System.out.print("Hand : ");
		this.displayHand();
		System.out.print("has type ");
		switch (this.HT.getHandType()) {
		case 9:
			System.out.println("Royal Flush.");
			break;
		case 8:
			if ((cards[4] == 'A') && (cards[0] == '2')) { // case A 2 3 4 5
				System.out.println("Straight Flush from " + cards[4] + " to " + cards[3] + ".");
			} else {
				System.out.println("Straight Flush from " + cards[0] + " to " + cards[4] + ".");
			}
			break;
		case 7:
			System.out.println("Four of a kind of " + cards[4] + ".");
			break;
		case 6:
			System.out.println("Full House with three " + cards[4] + " and a pair of " + cards[1] + ".");
			break;
		case 5:
			System.out.println("Flush with Highest card " + cards[4] + ".");
			break;
		case 4:
			if ((cards[4] == 'A') && (cards[0] == '2')) { // case A 2 3 4 5
				System.out.println("Straight from " + cards[4] + " to " + cards[3] + ".");
			} else {
				System.out.println("Straight from " + cards[0] + " to " + cards[4] + ".");
			}
			break;
		case 3:
			System.out.println("Three of a kind of " + cards[4] + ".");
			break;
		case 2:
			System.out.println("Two Pairs with one of " + cards[4] + " and one of " + cards[2] + ".");
			break;
		case 1:
			System.out.println("Pair of " + cards[4] + ".");
			break;
		case 0:
			System.out.println("Highcard : " + cards[4] + ".");
			break;
		default:
			break;
		}
	}
}
